import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPopularMovies } from '../actions/index';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

class MovieList extends Component {
  componentDidMount() {
    this.props.fetchPopularMovies();
  }

  renderMovieList(movies) {
    const results = movies.results.map(r => {
      return (
        <li className="col-xs-6" key={r.id}>
          <Link to={`/moviedetail/${r.id}`} params={{ id: r.id }}>
            <h3>{r.title}</h3>
            <img src={'https://image.tmdb.org/t/p/w200' + r.poster_path} alt=""/>
          </Link>
        </li>
      );
    });

    return results;
  }

  render() {
    let movieList;

    if (this.props.movieList.length < 1) {
      movieList = this.props.popularMovies.map(this.renderMovieList);
    } else {
      movieList = this.props.movieList.map(this.renderMovieList);
    }

    return (
      <ul className="row list-unstyled">
        {movieList}
      </ul>
    );
  }
}

function mapStateToProps({movieList, popularMovies}) {
  return { 
    movieList,
    popularMovies
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchPopularMovies }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieList);