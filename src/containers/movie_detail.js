import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMovieDetail } from '../actions/index';

class MovieDetail extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchMovieDetail(id);
  }

  render() {

    const movieDetails = this.props.movieDetail.data || {};

    console.log(movieDetails);
    return (
      <div>
        <h1>{ movieDetails.title }</h1>
        <img src={'https://image.tmdb.org/t/p/w400' + movieDetails.poster_path} alt={movieDetails.title}/>
        <p>{ movieDetails.overview }</p>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMovieDetail }, dispatch);
}

function mapStateToProps({movieDetail}) {
  return { movieDetail };
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);