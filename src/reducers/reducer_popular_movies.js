import { FETCH_POPULAR_MOVIES } from '../actions/index';

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_POPULAR_MOVIES:
      return [ action.payload.data ];
  }

  return state;
}