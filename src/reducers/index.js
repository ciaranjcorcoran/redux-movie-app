import { combineReducers } from 'redux';
import MovieListReducer from './reducer_movies';
import PopularMoviesReducer from './reducer_popular_movies';
import MovieDetailReducer from './reducer_movie_detail';

const rootReducer = combineReducers({
  movieList: MovieListReducer,
  popularMovies: PopularMoviesReducer,
  movieDetail: MovieDetailReducer
});

export default rootReducer;