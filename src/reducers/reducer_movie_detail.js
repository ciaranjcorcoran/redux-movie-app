import { FETCH_MOVIE_DETAIL } from '../actions/index';

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_MOVIE_DETAIL:
      return { data: action.payload.data };
  }

  return state;
}