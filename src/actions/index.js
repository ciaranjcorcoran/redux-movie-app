import axios from 'axios';

const API_KEY = '&api_key=2234cacce9b0984db6e042121f89f906';
const API_KEY_ALT = '?api_key=2234cacce9b0984db6e042121f89f906'
const ROOT_URL = `https://api.themoviedb.org/3/search/movie?query=`;
const URL_DETAIL = `https://api.themoviedb.org/3/movie/`;
const DEFAULT_URL = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc';

export const FETCH_POPULAR_MOVIES = 'FETCH_POPULAR_MOVIES';
export const FETCH_MOVIE_LIST = 'FETCH_MOVIE_LIST';
export const FETCH_MOVIE_DETAIL = 'FETCH_MOVIE_DETAIL';

export function fetchPopularMovies() {
  const url = DEFAULT_URL + API_KEY;
  const request = axios.get(url);

  return {
    type: FETCH_POPULAR_MOVIES,
    payload: request
  };
}

export function fetchMovieList(movie) {
  const url = ROOT_URL + movie + API_KEY;
  const request = axios.get(url);

  return {
    type: FETCH_MOVIE_LIST,
    payload: request
  };
}

export function fetchMovieDetail(movie) {
  const url = URL_DETAIL + movie + API_KEY_ALT; 
  const request = axios.get(url);

  return {
    type: FETCH_MOVIE_DETAIL,
    payload: request
  };
}